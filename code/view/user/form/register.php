<form method="POST" action="<?=$package->url('/register/');?>">

      <fieldset>
        <legend>
          Register
        </legend>

        
        <label for="email">Email:</label><br>
        <input type="email" name="email" required>
        <br><br>
        
        &nbsp;&nbsp;&nbsp;&nbsp;<label for="email_confirm">Confirm Email:</label><br>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="email" name="email_confirm" autocomplete="off" required>
        <br><br>
        <?=$lia->view('user/PasswordRequirements')?>
        <br>
        <label for="password">Password:</label><br>
        <input type="password" name="password">
        <br><br>

        &nbsp;&nbsp;&nbsp;&nbsp;<label for="password_confirm">Confirm Password:</label><br>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="password_confirm" autocomplete="off">
      </fieldset>
      <br>
        <label>
            By registering you agree to the <a href="<?=$package->url('/terms/')?>">Terms &amp; Conditions</a>. This includes logging of your email, IP Address, and User Agent for security purposes, even if registration is unsuccessful.<br>
        &nbsp;<input type="checkbox" name="agreed_to_terms" required>
            I agree to terms & conditions & security logging<br>
        </label>
      <br><br>
      
      <input type="submit" value="Create Account">
      <?=$lib->get_csrf_session_input('register')?>
      <?=$lia->view('user/form/honey');?>
  <br>
  
</form>
  
  <?=$lia->view('user/Links',['links'=>['login','reset.password','help']]);?>
