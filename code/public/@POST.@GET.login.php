<?php

if ($lib->page_is_disabled('login'))return;

$args = get_defined_vars();
$v = new \Tlf\User\Validation($args); 
// paths
// 0: already logged in, redirect (show message)
// 1: view login form
// 2: login successful, redirect (maybe with message)
// 3: login unsuccessful, show form with error message
// 4: already logged in, redirect with message

$csrf_args = ['login', 30, $package->url('/login/')];

$v->is_get()
    ->is_logged_out('You are already logged in. You must logout first to login to a different account.', [$lia,'view','user/Links', ['links'=>['logout','help']]])
    ->enable_csrf(...$csrf_args)
    ->show_form('login');
    ;

if ($v->state)return;

$show_form = [$v, 'show_csrf_form', 'login', ...$csrf_args];

$message = 'login failed. Email and/or password is incorrect.';

$v->is_post()
    ->check_honey("Failed to pass anti-spam checks. Please try again.", $show_form)
    ->throttle('login.user',$v->data['email']??uniqid(), 5000)
    ->throttle('login.ip',$_SERVER['REMOTE_ADDR'], 5000)
    ->check_csrf('login', '@csrf.invalid')
    ->post_email_is_valid("'".($v->data['email']??'')."' is not a valid email address. Please try again.", $show_form)
    ->log('login: attempt')
    ->post_email_account_active($message, $show_form)
    ->post_login($message, $show_form)
    ->log('login: success')
    ->goto('/')
    ;


?>

