<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/class/Backend.php  
  
# class useCartalyst\Sentinel\Native\Facades\Sentinel\Backend  
A wrapper for all things Sentinel, so this SHOULD be the ONLY place that has a reference to Sentinel  
  
  
## Constants  
  
## Properties  
- `protected $capsule;`   
- `protected $core;`   
- `protected $package;`   
  
## Methods   
- `public function __construct(\Lia\Compo $core)`   
- `public function getPdo()`   
- `public function connectDb()`   
- `public function getSqlCreate()`   
- `public function updateUserFirstName($model, $newFirstName)`   
- `public function updateUserLastName($model, $newLastName)`   
- `public function updateUserEmail($model, $newEmail)`   
- `public function deleteUser($model)`   
- `public function logoutUser()`   
- `public function login($model)`   
- `public function logoutEverywhere($model)`   
- `public function register($email, $name, $password)`   
- `public function setPassword($model, $newPassword)`   
- `public function getLoggedInUser()`   
- `public function userFromEmail($email)`   
- `public function authenticate($email, $password)`   
- `public function userById($id)`   
- `public function removeActivation($model)`   
- `public function newActivationCode($sentinelUser)`   
- `public function completeActivation($model, $code)`   
- `public function hasCompletedActivation($model)`   
- `public function getRoles($user)`   
- `public function getRoleFromSlug($roleName)`   
- `public function createRole($roleName)`   
- `public function addRole($user, $roleName)`   
- `public function getPermissions($user)`   
- `protected function permissionKey($action, $withId)`   
- `public function permit($user, $action, $withId=null)`   
- `public function revoke($user, $action, $withId=null)`   
- `public function forbid($user, $action, $withId=null)`   
- `public function can($user, $action, $withId=null)`   
- `public function isRole($user, $role)`   
  
