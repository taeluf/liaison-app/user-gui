<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Gui/Register.php  
  
# class Tlf\User\Gui\Register  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testCompleteExpiredRegistration()`   
- `public function testCompleteRegistration()`   
- `public function testInvalidEmail()`   
- `public function testInvalidConfirmationPassword()`   
- `public function testInvalidPassword()`   
- `public function testInvalidConfirmationEmail()`   
- `public function testUserExists()`   
- `public function testCreateUser()`   
- `public function testViewForm()`   
  
