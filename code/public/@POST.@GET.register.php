<?php

if ($lib->page_is_disabled('register'))return;

// Paths
// 1: view register form
// 2: submit register form, create new user
// 3: submit register form, user exists
// 4: submit register form, invalid confirmation email
// 5: submit register form, invalid password
// 6: submit register form, invalid confirm pasword


$args = get_defined_vars();
$args['code_type'] = 'registration';

$csrf_args = ['register', 30, $package->url('/register/')];

$v = new \Tlf\User\Validation($args); 

$v->is_get()
    ->is_logged_out('You are already logged in. You must logout first to register a new account.', [$lia,'view','user/Links', ['links'=>['logout','help']]])
    ->enable_csrf(...$csrf_args)
    ->show_form('register');
    ;

if ($v->state)return;

$show_form = [$v, 'show_csrf_form', 'register', ...$csrf_args];

$message = 'An email has been sent to '.$_POST['email'].'. Please check your email to finish registration.';


$v->is_post()
    ->check_honey("Failed to pass anti-spam checks. Please try again.", $show_form)
    ->throttle('register-email',$v->data['email']??uniqid(), 5000)
    ->throttle('register-ip',$_SERVER['REMOTE_ADDR'], 5000)
    ->check_csrf('register', '@csrf.invalid')
    ->agrees_to_terms("You must agree to the terms and conditions", $show_form)
    ->log('register: agreed to terms')
    ->post_email_is_valid("'".($v->data['email']??'')."' is not a valid email address. Please try again.", $show_form)
    ->post_email_confirmed("The confirmation email you entered does not match. Please try again.", $show_form)
    ->post_password_is_valid('The password you entered does not meet our minimum security requirements. Please try again.', $show_form)
    ->post_password_matches_confirm_password('The confirmation password you entered does not match. Please try again.', $show_form)
    ->post_email_account_notexists($message, [$v, 'send_email_register_account_exists'])
    ->send_email_register()
    ->log('register: success, email sent')
    ->show_message($message)
    ;

