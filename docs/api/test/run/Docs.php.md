<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Docs.php  
  
# class Tlf\User\Test\Docs  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function prepare()`   
- `public function testPermissions()`   
- `public function testServer()` registers a user, completes registration, and logs in  
assertions are pretty lazy since all this stuff is thoroughly tested by the main server tests.  
  
