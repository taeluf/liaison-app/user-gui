<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/core/Core.php  
  
# class Tlf\User\Core  
  
  
  
## Constants  
  
## Properties  
- `protected $backend;`   
  
## Methods   
- `public function onCreate()`   
- `public function onStart()`   
- `public function onEmit_Server_willDisplayContent($response)`   
- `public function apiBackend_tlf_user_getUserBackend()`   
- `public function apiGet_tlf_user_getUser()`   
- `public function apiGetRole_tlf_user_getUserRole($roleName)`   
- `public function apiGetPdo_tlf_user_getUserPdo()` return a PDO instance for the user database  
- `public function isEmailValid($email)` a VERY lenient (poor) email validation   
  
- `public function randomPassword()`   
  
