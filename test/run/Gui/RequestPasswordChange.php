<?php

namespace Tlf\User\Test;

class RequestPasswordChange extends \Tlf\User\GuiTester {

    ////////////
    //
    // Actions being tested here:
    // - GET password reset form
    // - POST password reset form (sends email to finish reset)
    // - GET complete reset form (enter new password)
    // - POST complete reset form  (update password in db)
    //
    // States being tested:
    // - user does not exist
    // - user is registered, but not activated
    // - user is registered and activated
    // - user is currently logged in vs not
    //
    //
    ////////////


    protected $page_message = 'An email has been sent to %s. Please check your email to finish resetting your password';


    public function testRegisteredNotActivatedUserResetPassword(){
        $lib = new \Tlf\User\Lib($this->pdo());
        $email = 'reed@notactive.reset.password';
        $user = $lib->user_from_email($email);
        $user->register('abc');
        $user = $lib->user_from_email($email);
        $this->test("verify user not active, but is registered");
        $this->is_true($user->is_registered());
        $this->is_false($user->is_active());


        $response = $this->post('/user/reset-password/',
            ['email'=>$email,
            'test_spoof_ip'=>'reqpasschange.registered.notactivated'
            ]
        );
        $this->str_contains(
            $response,
            sprintf($this->page_message, $email)
        );

        $mail_msg = $this->get_email();

        // user isn't active ... so we should provide a code to complete registration via email

// echo "\n\n\n-----------\n\n";
        // echo $mail_msg;
// echo "\n\n\n-----------\n\n";
        // echo $response;
            // exit;

        $ldb = new \Tlf\LilDb($this->pdo());
        $code_rows = $ldb->select('code',['user_id'=>$user->id, 'type'=>'registration']);
        $register_code = $code_rows[0]['code'];

        $code_rows = $ldb->select('code',['user_id'=>$user->id, 'type'=>'password_reset']);
        $reset_code = $code_rows[0]['code'];

        $register_url = 'http://create.localhost/user/complete.registration.'.$register_code.'/'; 
        $reset_url = 'http://create.localhost/user/complete.password_reset.'.$reset_code.'/';

        $this->str_contains($mail_msg,
            'You tried to reset your password, but you have not completed registration.',
            '1. Complete registration by visiting <a href="'.$register_url.'">'.$register_url.'</a>',
            '2. Then you can reset your password by visiting <a href="'.$reset_url.'">'.$reset_url.'</a>',
        );
    }

    /**
     * @test successfully requesting a reset
     */
    public function testSubmitReset(){
        $lib = new \Tlf\User\Lib($this->pdo());
        $email = 'reed@reset.password';
        $user = $this->get_active_user($email, 'abc');

        $response = $this->post('/user/reset-password/',
            ['email'=>$email,
            'test_spoof_ip'=>'reqpasschange.submit.reset'
            ]
        );

        // echo $response;
        // exit;

        $this->test("Response");
        $this->str_contains($response,sprintf($this->page_message, $email));

        $this->str_not_contains($response,'<form');


        $this->test("Email message");
        $msg = $this->get_email();
        $user = $lib->user_from_email($email);
        $ldb = new \Tlf\LilDb($this->pdo());
        $code_rows = $ldb->select('code',['user_id'=>$user->id, 'type'=>'password_reset']);
        $reset_code = $code_rows[0]['code'];
        $target_url = 'http://create.localhost/user/complete.password_reset.'.$reset_code.'/';
        $target_msg = "To setup a new password, visit <a href=\"$target_url\">$target_url</a>.";
        $this->compare($target_msg,$msg);

        // i will likely later write a test that is the entire flow


//
        // $login_code = $user->password_login('abc');
        // $this->is_string($login_code);
//
        // $complete_reset_form = $this->get('/complete.password_reset.'.$reset_code.'/');
//
//
        // $this->is_false($user->password_login('abc'));
    }

    public function testUserNotExists(){
        $response = $this->post('/user/reset-password/',
            ['email'=>'reed@user.notexists',
            'test_spoof_ip'=>'reqpasschange.user.not.exists'
            ]
        );

        $this->str_contains($response,
            sprintf($this->page_message, 'reed@user.notexists'),
        );

        $this->str_not_contains($response,
            '<form',
        );

        $target_url = 'http://create.localhost';
        $target_email = 'Someone tried to reset your password on <a href="'.$target_url.'">'.$target_url.'</a>, but you do not have an account with us. If this was you, please register with us at <a href="'.$target_url.'/user/register/">'.$target_url.'/user/register/</a>';
        $email = $this->get_email();
        $this->compare($target_email, $email);

    }

    public function testViewResetForm(){
        $response = $this->get('/user/reset-password/');

        $this->str_contains(
            $response,
            '<form method="POST" action="/user/reset-password/">'
        );


    }
}
