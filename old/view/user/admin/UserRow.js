
class UserRow extends Autowire {

    get emailNode(){return this.q('td:first-child');}
    get nameNode(){return this.q('td:nth-child(2)');}
    get surnameNode(){return this.q('td:nth-child(3)');}
    get deleteButton(){return this.q('td:nth-child(5) button');}
    get dialog(){return this.getAny('Dialog');}

    onAttach(){
        this.id = this.node.getAttribute('data-userid'); 

    }

    async onclick(event){
        this.newHandleClick(event.target);
    }

    async newHandleClick(node){
        if (!node.hasAttribute('api'))return;
        const params = {
            api:node.getAttribute('api'),
            user: this.id
        };
        for (const attr of node.attributes){
            const name = attr.name;
            if (!name.startsWith('api-'))continue;
            const key = name.substr(4);
            params[key] = attr.value;
        }
        const form = await this.ft('/user/admin/api/', params);
        this.dialog.show(form);
    }

}
UserRow.autowire('table.tlf-user.AdminTable tbody tr[data-userid]');
