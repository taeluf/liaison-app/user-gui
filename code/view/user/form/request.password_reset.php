

<form method="POST" action="<?=$package->url('/reset-password/');?>">

      <fieldset>
        <legend>
          Reset Password
        </legend>
        
        <label for="email">Email:</label><br>
        <input type="email" name="email" required>

      </fieldset>
      <br>

      
      <?=$lib->security_consent_box()?>

      <p>You will be sent an email to complete the password reset</p>
      <input type="submit" value="Reset Password">
      <?=$lib->get_csrf_session_input('request-password')?>
      <?=$lia->view('user/form/honey');?>
  <br>
  
</form>
  
  <?=$lia->view('user/Links',['links'=>['login','register','help']]);?>
