<h1>
  User Login Help
</h1>
<p>
  The password requirements are below. If you have any other questions, please visit the <a href="<?=$this->contact_url;?>">Support Page</a> for assistance.
</p>
  <?=$this->passwordRequirementsHtml()?>

<?=$this->links('login','logout','register','reset.password','account');