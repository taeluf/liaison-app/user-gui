<?php

if ($lib->page_is_disabled('register'))return;

// no csrf because this page is necessarily activated through a GET request from a link clicked in an email ... i don't love that, but i don't really see a workaround
// plus, this is not a dangerous thing to do. It's just a registration completion

// paths, step 1
// 1. user logged in currently. Code is valid. log user out. Proceed
// 2. user is not logged in currently. Code is valid. Proceed
// 3. user is logged in, code is not valid, show error
// 4. user is not logged in, code is not valid, show error


// throttle
$validation = new \Tlf\User\Validation(['lib'=>$lib, 'user'=>$user,]);

$validation->throttle('complete_registration-ip', $_SERVER['REMOTE_ADDR']);

if ($validation->state!==true)return false;


$action = 'registration';

$stmt = $lib->pdo->prepare(
    "SELECT u.email FROM `user` u
        JOIN `code` c
            ON c.user_id = u.id
    WHERE c.code LIKE :code
        AND c.type LIKE :type
        AND c.expires_at > NOW()
        AND c.is_active = 0
    "
);
$stmt->execute(
    ['code'=>$code,
    'type'=>$action
    ]
);

$rows = $stmt->fetchAll();

if (count($rows)!==1){
    $reset_password_url = $package->url('/reset-password/');
    $login_url = $package->url('/login/');

    echo "The code you submitted is not valid. If you've just clicked a link in your email, you may already be registered and can <a href=\"$login_url\">log in</a> or the code may have expired & you can <a href=\"$reset_password_url\">reset your password</a> to finish registration.";
    return;
}

$user = $lib->user_from_email($rows[0]['email']);
if (!$user->activate($code)){
    echo "Something went wrong activating your account.";
    $validation->log('register: error completing confirmation', $user->email, true);
    return;
}
$validation->log('register: completed email confirmation', $user->email, true);

?>
<h1>Registration complete!</h1>
<p><a href="<?=$package->url('/login/')?>">Login here!</a></p>

