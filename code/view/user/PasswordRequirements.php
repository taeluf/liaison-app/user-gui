<?php $lib = $package->lib; ?>
<details class="tlf-user">
<summary>Password Requirements</summary>

Passwords must be <strong><?=$lib->min_len?> characters or longer</strong> (up to <?=$lib->max_len?> characters).
<br>Passwords MUST include one of each:
<ul>
    <?=$lib->require_num ? '<li>A <b>number</b></li>' : '' ?>
    <?=$lib->require_mix_case ? '<li>A <b>capital letter</b></li>' : '' ?>
    <?=$lib->require_mix_case ? '<li>A <b>lower case letter</b></li>' : '' ?>
    <li>A <b>symbol</b>. At least one of: <b><?=htmlspecialchars($lib->password_symbols)?></b></li>
</ul>
<h4>Tips for a secure password</h4>
<p>An ideal password is 12+ characters. As technology improves, they need to be longer and longer to prevent hacking. You can use complete sentences so it's easier to remember or use a password manager to keep track of very long, random passwords. You may also make your own acronym from a sentence you come up with.</p>

</details>
