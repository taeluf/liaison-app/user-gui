<?php


// @TODO check if the currently logged in user is in 'admin' role

$api = $_POST['api'] ?? null;
if ($api==null)return;
$parts = explode('.',$api);
$class = '\\Api\\'.$parts[0];
$action = $parts[1];
$method = 'api'.ucfirst($action);
if (count($parts)>2)die("More than two parts are not currently allowed");

if (!class_exists($class)){
    die("Api '{$class}' doesn't exist.");
}

$obj = new $class($lia);
if (!method_exists($obj, $method)){
    die("Api action '$action' doesn't exist");
}


$data = (object)($_POST);
$args = $obj->apiArgs($data);
$specArgsMethod = $method.'Args';
if (method_exists($obj, $specArgsMethod)){
    $specArgs = $obj->$specArgsMethod(...[...$args, $data]);
    $args = [...$args, ...$specArgs];
}
$args[] = $data;
$obj->$method(...$args);

return;

$type = $_POST['api'] ?? null;
$form = $_POST['form'] ?? null;
$what = $_POST['what'] ?? null;
$userId = $_POST['user'] ?? null;

// right now, I have three different APIs. 
// form, update, delete
//
// But more acurately, they're user-form, user-update, and user-delete
//
// How would any of this work if I added a different domain, like role-update, role-form, etc...
// So I need to identify the user api PLUS the action on that api
// the api is user. the action is update, delete, form, etc.
//
// A simple way to do this is User.update, User.delete, & so on.
// It clutters the html MUCH less
//
// The (again, simple) solution is to have a class named 'User' inside a configured namespace.
// User will have methods apiUpdate, apiDelete, apiForm, & so on.
//
// Each apiWHATEVER method can have its own authentication/validation
// But the entire class can also have authentication/validation
//
//

$model = $lia->getUserBackend()->userById($userId);
if ($type=='form'){
    echo $lia->view('user/adminform/'.$form, ['user'=>$model, 'for'=>$_POST['for']??'user_name']);
} else if ($type=='update'&&$what=='user_name'){
    $lia->getUserBackend()->updateUserFirstName($model, $_POST['name']);
} else if ($type=='update'&&$what=='user_last_name'){
    $lia->getUserBackend()->updateUserLastName($model, $_POST['name']);
} else if ($type=='update'&&$what=='user_email'){
    $lia->getUserBackend()->updateUserEmail($model, $_POST['email']);
} else if ($type=='delete'&&$what=='user'){
    if (trim($_POST['confirm'])=='delete '.$model->email && $model->email != ''){
        $lia->getUserBackend()->deleteUser($model);
        echo 'User deleted!';
        return;
    }
    echo 'Could not delete user because your confirmation was invalid.';
}

