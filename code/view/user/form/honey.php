<?php

$names = [
    0=>uniqid(),
    1=>uniqid(),
    2=>'a'.uniqid().'b',
];

$answer = uniqid();
?>
<input type="text" name="<?=$names[0]?>" style="display:none;" value="" />
<input type="hidden" name="<?=$names[1]?>" value="" />
<br>
<br>
<div class="<?=$names[2]?>">Please type <b><?=$answer?></b> into here, or enable javascript:<br>
    <input type="text" name="<?=$names[2]?>">
</div>
<br>

<script type="text/javascript">
(function(){
    const elem = document.querySelector('.<?=$names[2]?>');
    elem.style.display = 'none';
    const input = document.querySelector('.<?=$names[2]?> > input');
    input.setAttribute('value', '<?=$answer?>');
    console.log(input);
})();
</script>

<input type="hidden" name="honey" value="<?=implode(',', $names);?>">
<input type="hidden" name="honey_answer" value="<?=password_hash($answer, PASSWORD_DEFAULT)?>">
