<?php  

namespace Tlf\User;  

  
class Tester extends \Tlf\Tester {  

    public function get_active_user($email, $password='abc'){
        $pdo = $this->pdo();
        // default errmode was changed from silent to exceptions in php 8.0. This is the easiest fix.
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_SILENT);
        $lib = new \Tlf\User\Lib($pdo);
        $user = $lib->user_from_email($email);
        $user->register($password);
        $code = $user->new_code('registration');
        $user->activate($code);
        return $user;
    }

    public function pdo(){
        static $pdo=null;
        if ($pdo==null){
            $settings = $this->settings();
            list("db"=>$db_name,"user"=> $user,"password"=> $password) = $settings;
            $pdo = new \PDO("mysql:dbname=$db_name", $user,$password);
        }
        return $pdo;
    }

    public function settings(){
        static $settings=null;
        if ($settings!=null)return $settings;
        $settings = json_decode(file_get_contents($this->file('test/db-env.json')),true);
        return $settings;
    }
  
    /** called before tests are run */  
    public function prepare(){
        $pdo = $this->pdo();
        // return;
        // empty the sentinel database
        $pdo->exec(
            "
            DROP TABLE IF EXISTS user;
            DROP TABLE IF EXISTS code;
            DROP TABLE IF EXISTS permissions;
            DROP TABLE IF EXISTS user_role;
            DROP TABLE IF EXISTS role_permission;
            DROP TABLE IF EXISTS throttle;
            DROP TABLE IF EXISTS security_log;
            ");
        $err = $pdo->errorInfo();
        $stmt = $pdo->query("SHOW TABLES");
        $all = $stmt->fetchAll(\PDO::FETCH_COLUMN);
        if (count($all)>0){
            echo "Tests cannot run because database failed to empty. \n See prepare() method";
            print_r($err);
            exit;
        }

        $lib = new \Tlf\User\Lib($pdo);
        $lib->init_db();
        // $settings = $this->settings();
    }  

}
