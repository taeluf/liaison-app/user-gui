<?php

if ($lib->page_is_disabled('reset-password'))return;


$args = get_defined_vars();
$args['code_type'] = 'password_reset';

$csrf_args = ['request-password', 30, $package->url('/reset-password/')];
$v = new \Tlf\User\Validation($args); 

$v->is_get()
    ->enable_csrf(...$csrf_args)
    ->show_form('request.password_reset')
    ;

if ($v->state)return;

$show_form = [$v, 'show_csrf_form', 'complete.password_reset', ...$csrf_args];

$message = 'An email has been sent to '.$_POST['email'].'. Please check your email to finish resetting your password.';

$v->is_post()
    ->check_honey("Failed to pass anti-spam checks. Please try again.", $show_form)
    ->throttle('request-new-password-email',$v->data['email']??uniqid(), 5000)
    ->throttle('request-new-password-ip',$_SERVER['REMOTE_ADDR'], 5000)
    ->check_csrf('request-password', '@csrf.invalid')
    ->post_email_is_valid("'".($v->data['email']??'')."' is not a valid email address. Please try again.", $show_form)
    ->log('password reset: requested')
    ->post_email_account_exists($message, [$v, 'send_email_request_reset_account_notexists'])
    ->post_email_account_active($message, [$v, 'send_email_request_reset_account_inactive'])
    ->send_email_request_reset_password()
    ->log('password reset: email sent')
    ->show_message($message)
    ;

