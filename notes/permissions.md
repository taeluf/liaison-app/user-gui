# permissions idea
This was an old idea for how to do permissions. My current setup is much simpler, but this would, perhaps, be more robust. I don't really plan to implement this.


Add an item ownership table. Something like:
```
uuid 1    | relationship | uuid 2       | table1      | table2     
 {AF32NK}      owns        {DUI3289}      users          BlogPosts
```

And its just:
```php
<?php
    $user->owns($otherItem); //bool true/false
    $user->relate('owns', $otherItem);
    $user->relate('moderator of', $otherItem);
    $otherItem->isOwnedBy($user);
    $otherItem->isRelatedBy($user, 'moderator of');
```
