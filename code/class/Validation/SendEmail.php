<?php

namespace Tlf\User\Validation;

/**
 * Methods for sending emails
 */
trait SendEmail {

    public function show_mail_failed_send($form=null){

            echo "<p class=\"error\">There was an internal error sending the email for password reset. Please try again.</p>";
            if ($form!=null)$this->show_form($form);
            return new \Tlf\User\BlackHole($this);
    }

    public function send_security_email(){
        $lib = $this->lib;

        $website = $lib->config['web_address'];
        $msg = "Your password for <a href=\"$website\">$website</a> was changed.";
        $mail_sent = mail($_POST['email'], 'Password Updated', $msg,
            [
                "From"=>$lib->config['email_from'],
                "Reply-To"=>$lib->config['email_from']
            ],
        );
        if ($mail_sent)return $this;
        else {
            echo '<p class="error">Internal Error: Failed to send security email, so password cannot be updated. Please try again.</p>';
            echo $this->show_form('complete.password_reset');
            return new \Tlf\User\BlackHole($this);
        }
    }

    public function send_email_request_reset_account_notexists(){
        $lib = $this->lib;
        $package = $this->args['package'];
        $website = $lib->config['web_address'];
        $register_url = $website.$package->url('/register/');
        $msg = 'Someone tried to reset your password on <a href="'.$website.'">'.$website.'</a>, but you do not have an account with us. If this was you, please register with us at <a href="'.$register_url.'">'.$register_url.'</a>';


        $mail_sent = mail($this->data['email'], 'Password Reset', $msg,
            [
                "From"=>$lib->config['email_from'],
                "Reply-To"=>$lib->config['email_from']
            ],
        );
        if (!$mail_sent)return $this->show_mail_failed_send('complete.password_reset');

        return $this;
    }

    public function send_email_request_reset_account_inactive(){
        $lib = $this->lib;
        $package = $this->args['package'];
        $email = $this->data['email'];

        $user = $lib->user_from_email($email);
        $register_code = $user->new_code('registration');
        $website = $lib->config['web_address'];
        $reset_code = $user->new_code('password_reset');

        $register_url = $website.$package->url('/complete.registration.'.$register_code.'/');
        $reset_url = $website.$package->url("/complete.password_reset.$reset_code/");
        $mail_msg = "You tried to reset your password, but you have not completed registration."
                ."<br>\n".'1. Complete registration by visiting <a href="'.$register_url.'">'.$register_url.'</a>'
                ."<br>\n".'2. Then you can reset your password by visiting <a href="'.$reset_url.'">'.$reset_url.'</a>';


        $mail_sent = mail($email, 'Password Reset', $mail_msg,
            [
                "From"=>$lib->config['email_from'],
                "Reply-To"=>$lib->config['email_from']
            ],
        );


        if (!$mail_sent)return $this->show_mail_failed_send('complete.password_reset');

        return $this;
    }


    public function send_email_register_account_exists(){

        $lib = $this->lib;
        $package = $this->args['package'];
        $url = $lib->config['web_address'].$package->url('/login/');
        $mail_sent = mail($_POST['email'], 'Account Registration',
        "Somebody tried to register an account with your email address, but you already have an account with us. Log in at "
        ."<a href=\"$url\">$url</a>",
            [
                "From"=>$lib->config['email_from'],
                "Reply-To"=>$lib->config['email_from']
            ],
        );
        if (!$mail_sent)return $this->show_mail_failed_send('register');
        return $this;
    }

    public function send_email_request_reset_password(){
        $lib = $this->lib;
        $package = $this->args['package'];
        $email = $this->data['email'];
        $website = $lib->config['web_address'];
        $user = $lib->user_from_email($email);
        $reset_code = $user->new_code('password_reset');
        $reset_url = $website.$package->url('/complete.password_reset.'.$reset_code.'/');
        $mail_msg = "To setup a new password, visit <a href=\"$reset_url\">$reset_url</a>.";

        // var_dump($lib->config['email_from']);
        // var_dump($email);
        // exit;


        $mail_sent = mail($email, 'Password Reset', $mail_msg,
            [
                "From"=>$lib->config['email_from'],
                "Reply-To"=>$lib->config['email_from']
            ],
        );
        if (!$mail_sent)return $this->show_mail_failed_send('complete.password_reset');

        return $this;
    }


    public function send_email_register(){
        $lib = $this->lib;
        $package = $this->args['package'];
        $email = $this->data['email'];

        $user = $lib->user_from_email($email);
        $user->register($_POST['password']);
        $code = $user->new_code('registration');
        $register_url = $lib->config['web_address'].$package->url('/complete.registration.'.$code.'/');
        // generate code
        // build url
        $mail_sent = mail($_POST['email'], 'Account Registration',
            "To complete registration, visit <a href=\"$register_url\">$register_url</a>",
            ['From'=>$lib->config['email_from'],
            'Reply-To'=>$lib->config['email_from']
            ],
        );

        if (!$mail_sent)return $this->show_mail_failed_send('register');
        return $this;
    }
}
