<?php

list('id'=>$id, 'email'=>$email, 'first_name'=>$first_name, 'last_name'=>$last_name) = (array)$user;

// $strRoles = implode(', ', $roles);
$strRoles = $roles;
?>

<tr data-userid="<?=$id?>">
    <td api="User.form" api-form="UserEmail"><?=$email?></td>
    <td api="User.form" api-form="UserName"><?=$first_name?></td>
    <td api="User.form" api-form="UserName" api-for="user_last_name"><?=$last_name?></td>
    <td><?=$strRoles?></td>
    <td><button api="User.form" api-form="DeleteUser">Delete</button></td>
</tr>
