<?php 

namespace Tlf\User\Test\Gui;

class Gui extends \Tlf\User\GuiTester {

    public function testDisablePages(){
        $urls = ['login','register','reset-password','logout','terms'];
        foreach ($urls as $page){
            $this->test('Page: '.$page);
            $content = $this->get("/user/$page/",['disable_pages'=>true]);
            // echo $content;
            // exit;
            $this->str_contains(
                $content,
                '<h1>Page Disabled</h1>',
            );
            $this->str_not_contains(
                $content,
                '<form',
            );
        }
    }

    public function testGuiLogout(){
        $email = 'reed@test.logout';
        $cookie = $this->login($email);
        $response = $this->cookie_get('/user/logout/', $cookie);
        // echo $response;

        $this->str_contains($response,
            '<p>You have been logged out. Have a great day!</p>',
        );

        $lib = new \Tlf\User\Lib($this->pdo());

        $user = $lib->user_from_cookie($cookie);
        $this->is_false($user);

        $already_logged_out = $this->cookie_get('/user/logout/', $cookie);

        $this->str_contains(
            $already_logged_out,
            '<form method="POST" action="/user/login/"',
        );

    }
}
