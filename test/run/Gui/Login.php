<?php

namespace Tlf\User\Test\Gui;

class Login extends \Tlf\User\GuiTester {

    public function testThrottleLoginIp(){
        $path = '/user/login/';
        $password = 'abc';
        $wrong_pass = 'def';
        $email = 'reed@post.throttle.login';
        $user = $this->get_active_user($email, $password);
        $params = [
            'email'=>$email,
            'password'=>$wrong_pass,
            'test_spoof_ip'=>$this->spoof('post.throttle.ip'),
        ];
        $login_response = $this->post($path, $params);
        $params_right = [];
        $params_right['email'] = $email.'.abc';
        $params_right['password'] = $password;
        $params_right['test_spoof_ip']=$this->spoof('post.throttle.ip');
        $throttle_response = $this->curl_post($path, $params_right);

        echo var_dump($throttle_response['body']);

        $this->test('there should be no cookies or redirect');
        $this->is_true(empty($throttle_response['cookies']['taeluf_login']));
        $this->is_true(empty($throttle_response['headers']['Location']));

        $this->str_contains(
            $throttle_response['body'],
            "Please wait 5 seconds before trying again.",
        );

    }

    public function testThrottleLoginUser(){
        $path = '/user/login/';
        $password = 'abc';
        $wrong_pass = 'def';
        $email = 'reed@post.throttle.login';
        $user = $this->get_active_user($email, $password);
        $params = [
            'email'=>$email,
            'password'=>$wrong_pass,
            'test_spoof_ip'=>$this->spoof('post.throttle.login'),
        ];
        $login_response = $this->post($path, $params);
        // echo $login_response;
        // exit;
        $params_right = $params;
        $params_right['password'] = $password;
        $params_right['test_spoof_ip']=$this->spoof('post.throttle.login2');
        $throttle_response = $this->curl_post($path, $params_right);

        echo var_dump($throttle_response['body']);

        $this->test('there should be no cookies or redirect');
        $this->is_true(empty($throttle_response['cookies']['taeluf_login']));
        $this->is_true(empty($throttle_response['headers']['Location']));

        $this->str_contains(
            $throttle_response['body'],
            "Please wait 5 seconds before trying again.",
        );

    }

    public function testAlreadyLoggedIn(){
        $password = 'onetowthree';
        $user = $this->get_active_user('reed@post.already.loggedin', $password);
        $path = '/user/login/';
        $params = [
            'email'=>'reed@post.already.loggedin',
            'password'=>$password,
            'test_spoof_ip'=>$this->spoof('post.already.loggedin'),
        ];
        $login_response = $this->curl_post($path, $params);

        $code = $login_response['cookies']['taeluf_login']['value'];

        $this->is_true($code!='deleted');

        // echo $code;
        // exit;

        $body = $this->cookie_get($path, $code);

        // echo $response;
        // exit;

        $this->str_contains($body,
            'You are already logged in. You must logout first to login to a different account.',
            '<html>',
        );
        $this->str_not_contains($body,
            '<form method="POST" action="/user/login/">',
            '<a href="/user/register/">',
        );
        $this->is_true(empty($headers['Location']));

    }

    public function testFailLogin(){

        $password = 'onetowthree';
        $user = $this->get_active_user('reed@post.fail.login', $password);
        $path = '/user/login/';
        $params = [
            'email'=>'reed@post.fail.login',
            'password'=>'wrong-password',
            'test_spoof_ip'=>$this->spoof('post.fail.login'),
        ];
        $response = $this->curl_post($path, $params);

        $body = $response['body'];
        $this->str_contains($body,
            'Email and/or password is incorrect.',
            '<html>',
            '<form method="POST" action="/user/login/">',
            '<a href="/user/register/">',
        );
        $this->is_true(empty($headers['Location']));

        // echo $body;

    }

    public function testSucceedLogin(){
        // how do i pass this test?
        // successful login yields redirect
            // verify redirect header
        // sends cookies
            // retrieve cookie code
            // get user object with the cookie code
            // check the remaining time on the cookie code
            // verify the cookie code expiration matches (or nears) the sent cookie expiration
        $password = 'abcdefghi';
        $user = $this->get_active_user('reed@post.login', $password);

        $path = '/user/login/';
        $params = [
            'email'=>'reed@post.login',
            'password'=>'abcdefghi',
            'test_spoof_ip'=>$this->spoof('post.login'),
        ];

        $response = $this->curl_post($path, $params);

        // print_r($response);
        // exit;
        $headers = $response['headers'];

        $this->compare(
            '/user/',
            $headers['Location'],
        );

        $cookies = $response['cookies'];
        $cookie = $cookies['taeluf_login'];
        print_r($cookies);

        $this->compare('taeluf_login',$cookie['name']);
        $code = $cookie['value'];

        $lib = new \Tlf\User\Lib($this->pdo());
        $cookie_user = $lib->user_from_cookie($code);
        $this->compare(
            $user->email,
            $cookie_user->email,
        );

        // verify the expiration DAY is correct. The time doesn't really matter
        $stmt = $this->pdo()->query("SELECT expires_at FROM code WHERE `code` LIKE '$code';");
        $code_expiry = $stmt->fetch()['expires_at'];

        $length = 60*60*24*180;
        $date = date_create();
        $date->setTimestamp(time() + $length);

        $target_expiration = $date->format('Y-m-d');
        $this->compare(
            $target_expiration,
            substr($code_expiry, 0,10),
        );

        //format: Tue, 30-Jun-1970 00:00:00 GMT
        $legacy_format = 'D, d-M-Y H:i:s e';
        $php82_format = 'D, d M Y H:i:s e';
        $cookie_expires = date_create_from_format(
            $legacy_format,
            trim($cookie['expires'])
        );

        if ($cookie_expires==false){
            $cookie_expires = date_create_from_format(
                $php82_format,
                trim($cookie['expires'])
            );
        }

        $this->compare(
            $target_expiration,
            $cookie_expires->format('Y-m-d')
        );

    }

    public function testViewLogin(){
        $content = $this->get('/user/login/');
        $this->str_contains($content,
            '<html>',
            '<form method="POST" action="/user/login/">',
            '<a href="/user/register/">',
        );
        echo $content;
    }
}
