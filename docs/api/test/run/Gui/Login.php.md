<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Gui/Login.php  
  
# class Tlf\User\Test\Gui\Login  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testThrottleLoginIp()`   
- `public function testThrottleLoginUser()`   
- `public function testAlreadyLoggedIn()`   
- `public function testFailLogin()`   
- `public function testSucceedLogin()`   
- `public function testViewLogin()`   
  
