<form action="/user/admin/api/" method="POST">
    <p>To delete this user, type <strong>delete <?=$user->email?></strong></p>
    <input type="text" name="confirm" placeholder="delete <?=$user->email?>" />
    <input type="hidden" name="user" value="<?=$user->id?>">
    <input type="hidden" name="api" value="User.delete">
    <input type="submit" value="Delete">
</form>
