# User Login
A User login system built with @hard_link(https://tluf.me/php/liaison, Liaison), which can be plugged into any existing website, whether you use Liaison or not.

**Warning:** The documentation is ... incomplete & just kinda bad right now.
**Legal:** YOU ARE RESPONSIBLE FOR TERMS & CONDITIONS & legal stuff! If you use this software, you must read & understand them. If you do not agree with them or do not want to use them, then you must replace them. If what i've written is not legally sufficient, YOU ARE STILL LIABLE & I take no responsibility for any legal matters pertaining to your use of this software, user privacy, etc.

## Status: Alpha, not feature complete (May 6, 2022)
This library has the essentials for user login: login, password reset, registration. It also has necessary features like terms & conditions & security stuff. It has CSRF, XSS prevention, honeypots, throttling, and security logs.

A big drawback currently is the lack of a GUI for user management, administration, and roles/permissions. This must be done in code, unfortunately. Though the back-end API is pretty easy to use so it's not that bad.

There are robust tests for most of the library. However, my CSRF is minimal & honeypot testing is non-existent (aside from trying it in the browser).

Setup is a bit cumbersome. Documentation is currently poor. 

I plan to add some new cli commands & a convenience method for standard setups on your server.

## Install
@template(php/composer_install, taeluf/user-gui)

## Sever Setup
This is a simple setup for a Liaison server. 

If you wish to use this on a non-liaison website, then instead of `$liaison->deliver()`, call `$liaison->getResponse()`, get the content & headers from the response & do with them as you need.

```php
@file(test/DocServer/deliver.php)
```

### Database setup
Uncomment the `$lib->init_db()` line in the deliver script above, then run the deliver script, then re-comment the `init_db` line

### Permissions and Roles
Permissions can be added to users and to roles. Roles can then be added to users to grant a group of permissions. See permissions source code at @see_file(code/class/Permissions.php).

Examples:
```php
<?php
@ast(class.Tlf\User\Test\Docs.methods.testPermissions.body)
```


## Database Schema
This is the file: @see_file(code/db/create.sql).
```sql
@file(code/db/create.sql)
```
