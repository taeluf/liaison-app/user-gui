<form method="POST" action="<?=$package->url('/complete.password_reset.'.$code.'/');?>">

      <fieldset>
        <legend>
          Set new password
        </legend>

        
        <label for="email">Your email:</label><br>
        <input type="email" name="email" required>
        <br><br>
        
        <?=$lia->view('user/PasswordRequirements')?>
        <label for="password">New Password:</label><br>
        <input type="password" name="password">
        <br><br>

        &nbsp;&nbsp;&nbsp;&nbsp;<label for="password_confirm">Confirm New Password:</label><br>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="password_confirm" autocomplete="off">
      </fieldset>
      <br>
      <?=$lib->security_consent_box()?>

      <input type="submit" value="Set New Password">
      <?=$lib->get_csrf_session_input('complete-password')?>
      <?=$lia->view('user/form/honey');?>
  <br>
  
</form>
  
  <?=$lia->view('user/Links',['links'=>['login','register','help']]);?>
