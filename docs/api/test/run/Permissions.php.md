<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Permissions.php  
  
# class Tlf\User\Test\Permissions  
  
  
  
## Constants  
  
## Properties  
- `public $timing_user;` a user initiated in prepare() so we can easily benchmark JUST checking perms  
  
## Methods   
- `public function prepare()`   
- `public function testPermLists()`   
- `public function testRoleDeny()`   
- `public function testRoleDelete()`   
- `public function testRoleAllowTiming()`   
- `public function testRoleDenyTiming()`   
- `public function testHasRole()`   
- `public function testRoleAllow()`   
- `public function testDeny()`   
- `public function testAllow()`   
  
