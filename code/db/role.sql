-- @query(deny)
DELETE FROM role_permission WHERE role LIKE :role AND action LIKE :permission;


-- @query(delete, ; --stop)
DELETE FROM role_permission where role LIKE :role;
DELETE FROM user_role where role LIKE :role; --stop

-- @query(allow)
INSERT INTO role_permission (role, action)
VALUES (:role, :permission);

