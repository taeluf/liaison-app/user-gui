<?php

namespace Tlf\User\Validation;

/**
 * For testing email
 */
function mail(string $to,string $subject,string $body,array $headers,array $params=[]){

    $content = $body;
    if (filter_var($to, FILTER_VALIDATE_EMAIL)===false){
        $content = 'email '.$to.' is not a valid email.';
    }
    if (!isset($headers['From'])){
        $content = "'From' header was not set";
    }
    if (!isset($headers['Reply-To'])){
        $content = "'Reply-To' header was not set";
    }

    file_put_contents(
        __DIR__.'/email-body-out.txt',
        $content
    );

    error_log("\n\nFake email to '$to'");

    if ($content!==$body)return false;
    return true;
}
