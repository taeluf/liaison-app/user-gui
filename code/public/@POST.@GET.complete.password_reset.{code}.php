<?php

if ($lib->page_is_disabled('reset-password'))return;

$args = get_defined_vars();
$args['code_type'] = 'password_reset';

$v = new \Tlf\User\Validation($args); 

$csrf_args = ['complete-password', 30, $package->url("/complete.password_reset.$code/")];

$v->is_get()
    ->throttle('set-password-code',$code, 5000)
    ->throttle('set-password-ip',$_SERVER['REMOTE_ADDR'],5000)
    ->enable_csrf(...$csrf_args)
    ->code_is_valid("Password reset code '$code' is not valid.")
    ->code_user_is_active('Cannot complete password reset at this time because registration is not complete for your account.</p>'
        ."\n<p>Please check your email for an account registration email with a link to complete registration."
    )
    ->show_form('complete.password_reset')
    ;

// print_r($_SESSION);
// exit;

if ($v->state)return;

$show_form = [$v, 'show_csrf_form', 'complete.password_reset', ...$csrf_args];
$v->is_post()
    ->check_honey("Failed to pass anti-spam checks. Please try again.", $show_form)
    ->throttle('set-password-code',$code, 5000)
    ->throttle('set-password-email',$v->data['email']??uniqid(), 5000)
    ->throttle('set-password-ip',$_SERVER['REMOTE_ADDR'], 5000)
    ->check_csrf('complete-password', '@csrf.invalid')
    ->code_is_valid("Password reset code '$code' is not valid.")
    ->post_email_is_valid("'".($v->data['email']??'')."' is not a valid email address. Please try again.", $show_form)
    ->post_password_is_valid('The password you entered does not meet our minimum requirements. Please try again.', $show_form)
    ->post_password_matches_confirm_password('The confirmation password you entered did not match.', $show_form)
    ->code_email_matches_post_email('We could not update your password. Either the code you\'re using is invalid or you\'ve entered the wrong email address. Please try again.', $show_form)
    ->log('new password: attempt to set')
    ->code_user_is_active('Cannot complete password reset at this time because registration is not complete for your account. Please check your email for a registration confirmation link.')
    ->send_security_email()
    ->set_new_password('Internal Error: Unable to update your password. Please try again or contact support.')
    ->log('new password: successfully set, security email sent')
    ->show_message('Your password has been updated! Please <a href="/user/login/">Log in here</a> with your new password.');

